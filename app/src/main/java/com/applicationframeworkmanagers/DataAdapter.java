package com.applicationframeworkmanagers;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;


import com.applicationframeworkmanagers.databinding.DataRowBinding;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private DataRowBinding rowBinding;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<BluetoothDevice> dataArrayList;
    private OnItemClickListener onItemClickListener;
    public DataAdapter(final Context context, final ArrayList<BluetoothDevice> dataArrayList){
        this.context=context;
        this.dataArrayList=dataArrayList;

    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater=LayoutInflater.from(context);
        rowBinding= DataBindingUtil.inflate(inflater, R.layout.data_row,parent,false);
        return new ViewHolder(rowBinding);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position) {
        holder.rowAdapterBinding.rowName.setText(dataArrayList.get(position).getName());
        holder.rowAdapterBinding.rowRange.setText(dataArrayList.get(position).getAddress());


    }
    public void addDevice(BluetoothDevice device) {
        if(!dataArrayList.contains(device)) {
            dataArrayList.add(device);
        }
    }
    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        DataRowBinding rowAdapterBinding;
        public ViewHolder(DataRowBinding rowAdapterBinding) {
            super(rowAdapterBinding.getRoot());
            this.rowAdapterBinding=rowAdapterBinding;
            rowAdapterBinding.rowName.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListener!=null){
                onItemClickListener.onItemClick(v,getAdapterPosition());
            }
        }
    }
    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener=onItemClickListener;
    }
}
