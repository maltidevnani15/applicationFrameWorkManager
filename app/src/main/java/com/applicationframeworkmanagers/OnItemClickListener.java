package com.applicationframeworkmanagers;

import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;

/**
 * Created by Malti on 3/18/2018.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
