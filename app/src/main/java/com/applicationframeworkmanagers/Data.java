package com.applicationframeworkmanagers;

/**
 * Created by Malti on 2/18/2018.
 */

public class Data {
    private String name;
    private String range;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }


}

